from django.db import models
from ..users.models import User

class JobManager(models.Manager):
    def validate_job(self, form_data):
        errors=[]

        if len(form_data['title'])<3:
            errors.append("Please use at least 3 characters for title")
        if len(form_data['description'])<3:
            errors.append("Please use at least 10 characters for description")
        if len(form_data['location'])<1:
            errors.append("Please enter full address")
        
        return errors

    def create_job(self, id, form_data):
        return self.create(
            title = form_data['title'],
            description = form_data['description'],
            location = form_data['location'],
            posted_by = id
        )

    def update_job(self, id, form_data):
        job = self.get(id=id)
        
        if form_data['title'] != job.title:
            job.title = form_data['title']
        if form_data['description'] != job.description:
            job.description = form_data['description']
        if form_data['location'] != job.location:
            job.location = form_data['location']
        
        job.save()
        return job

    def capture_job(self, id, user_id):
        job = self.get(id=id)
        user = User.objects.get(id=user_id)
        job.assignee = user
        job.save()
        return job



class Job(models.Model):
    title = models.CharField(max_length=255)
    description = models.CharField(max_length=500)
    location = models.CharField(max_length=500)
    posted_by = models.CharField(max_length=255)
    assignee = models.ForeignKey(User, related_name="jobassign", null=True)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = JobManager()