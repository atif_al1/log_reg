from django.shortcuts import render, redirect
from django.contrib import messages
from ..jobs.models import Job
from .models import *

def index(req):
    return render(req, "users/index.html")

def process(req):
    errors = User.objects.validate(req.POST)
    if len(errors):
        for error in errors:
            messages.error(req, error)
        return redirect('/')
    user_new = User.objects.create_user(req.POST)
    req.session['user_id'] = user_new.id
    return redirect('/dashboard/')

def login(req):
    valid, result = User.objects.login_valid(req.POST)
    if not valid:
        for error in result:
            messages.error(req, error)
        return redirect('/')
    req.session['user_id'] = result.id
    return redirect('/dashboard/')

def dashboard(req):
    id = req.session['user_id']
    content = {
        "first_name": User.objects.get(id=id).first_name,
        "last_name": User.objects.get(id=id).last_name,
        "user" : str(id),
        "jobs": Job.objects.all(),
        "myjobs" : User.objects.get(id=id).jobassign.all()
    }
    return render(req, "users/dashboard.html", content)

def newjob(req):
    return render(req, "users/addjob.html")

def addjob(req):
    errors = Job.objects.validate_job(req.POST)
    if len(errors):
        for error in errors:
            messages.error(req, error)
        return redirect('/newjob/')

    id = req.session['user_id']
    new_job = Job.objects.create_job(id, req.POST)
    return redirect('/dashboard/')

def dojob(req, id):
    user_id = req.session['user_id']
    Job.objects.capture_job(id, user_id)
    return redirect('/dashboard/')

def edit(req, id):
    req.session['job_id'] = id
    content = {
        "id": req.session['job_id'],
        "title" : Job.objects.get(id=id).title,
        "description" : Job.objects.get(id=id).description,
        "location" : Job.objects.get(id=id).location,
    }
    return render(req, "users/edit.html", content)

def verify(req, id):
    id = id
    errors = Job.objects.validate_job(req.POST)
    if len(errors):
        for error in errors:
            messages.error(req, error)
        return redirect('/edit/'+str(id)+'/')
    Job.objects.update_job(id, req.POST)
    return redirect('/dashboard/')

def view(req, id):
    id = id
    user_id = Job.objects.get(id=id).posted_by
    content = {
        "title" : Job.objects.get(id=id).title,
        "description" : Job.objects.get(id=id).description,
        "location" : Job.objects.get(id=id).location,
        "posted_by" : User.objects.get(id=user_id).first_name+" "+User.objects.get(id=user_id).last_name,
        "created_at" : Job.objects.get(id=id).created_at,
        "id": id
    }
    return render(req, "users/view.html", content)

def destroy(req, id):
    Job.objects.get(id=id).delete()
    return redirect('/dashboard/')
    
def logout(req):
    req.session.clear()
    return redirect('/')
