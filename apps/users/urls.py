from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^$', views.index),
    url(r'^process/$', views.process),
    url(r'^login/$', views.login),
    url(r'^dashboard/$', views.dashboard),
    url(r'^verify/(?P<id>\d+)/$', views.verify),
    url(r'^newjob/$', views.newjob),
    url(r'^addjob/$', views.addjob),
    url(r'^dojob/(?P<id>\d+)/$', views.dojob),
    url(r'^edit/(?P<id>\d+)/$', views.edit),
    url(r'^view/(?P<id>\d+)/$', views.view),
    url(r'^cancel/(?P<id>\d+)/$', views.destroy),
    url(r'^logout/$', views.logout)
]