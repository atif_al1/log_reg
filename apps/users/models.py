from django.db import models
from ..jobs import *
import bcrypt
import re

NAME_REGEX = re.compile(r'^[a-zA-Z][^\d]+$')
EMAIL_REGEX = re.compile(r'^[a-zA-Z0-9.+_-]+@[a-zA-Z0-9._-]+\.[a-zA-Z]+$')

class UserManager(models.Manager):
    def validate(self, form_data):
        errors=[]

        if len(form_data['first_name'])<2:
            errors.append("Please enter a valid first name")
        elif not NAME_REGEX.match(form_data['first_name']):
            errors.append("Please use only letters")
        if len(form_data['last_name'])<2:
            errors.append("Please enter a valid last name")
        elif not NAME_REGEX.match(form_data['last_name']):
            errors.append("Please use only letters")
        if not EMAIL_REGEX.match(form_data['email']):
            errors.append("Please use a valid email")
        elif self.filter(email = form_data['email']):
            errors.append("Email already in use")
        if len(form_data['password'])<8:
            errors.append("Please enter a password at least 8 characters long")
        if form_data['password'] != form_data['confirm_pw']:
            errors.append("Passwords do not match")
        
        return errors
        
    def create_user(self, form_data):
        pw_hash = bcrypt.hashpw(form_data['password'].encode(), bcrypt.gensalt())
        return self.create(
             first_name = form_data['first_name'],
             last_name = form_data['last_name'],
             email = form_data['email'],
             pw_hash = pw_hash,
        )

    def login_valid(self, form_data):
        errors=[]
        try:
            user = self.get(email=form_data['email_login'])
        except:
            errors.append("Email or Password is invalid")
            return(False, errors)

        if bcrypt.checkpw(form_data['pw_login'].encode(), user.pw_hash.encode()):
            return(True, user)
        else:
            errors.append("Email or Password is invalid")
            return(False, errors)
        
class User(models.Model):
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    email = models.CharField(max_length=255)
    pw_hash = models.CharField(max_length=500)
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    objects = UserManager()

    def __str__(self):
        return self.first_name